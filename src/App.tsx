import React from 'react';
import "./App.css"
import EstimateTable from './Table/EstimateTable';
import PaymentTable from './Table/PaymentTable';

function App() {
  return (
    <>
      <EstimateTable />
      <PaymentTable />
    </>
  );
}

export default App;
