export const ESTIMATEDATA = [
    { label: "견적번호", value: "EST-20230607-126" },
    { label: "견적 요청 제목", value: "테이블 테스트" },
    { label: "가상 견적 요청일", value: "2023.06.07" },
    { label: "작업 품목", value: "패션슈즈 > 캔버스" },
    { label: "작업 수량", value: "12344 족" },
    { label: "납품 희망일", value: "2023.08.24" },
    {
        label: "첨부파일",
        value: [
            {imgName: "테스트이미지.jpg", url: "https://sinple-backend-bucket-dev.s3.ap-northeast-2.amazonaws.com/estimate/thumbnail/%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%8B%E1%85%B5%E1%84%86_UlwxPDj.jpg"},
            {imgName: "이미지테스트.jpg", url: "https://sinple-backend-bucket-dev.s3.ap-northeast-2.amazonaws.com/estimate/thumbnail/%E1%84%92%E1%85%B4%E1%86%AE%E1%84%83%E1%85%AE%E1%86%BC%E1%84%8B%E1%85%B5.jpeg"},
            {imgName: "testimg.jpg", url: "https://sinple-backend-bucket-dev.s3.ap-northeast-2.amazonaws.com/estimate/thumbnail/%E1%84%91%E1%85%A1%E1%84%8B%E1%85%B5%E1%86%AF%E1%84%8B%E1%85%B5%E1%84%86%E1%85%B5%E1%84%8C%E1%85%B5.png"},
        ]
    },
    { label: "기타 요청사항", value: "요청사항" },
]

export const PAYMENTDATA = [
    { label: "1차 결제금액(생산비용의 30%)", value: "18679374" },
    { label: "VAT", value: "1867937" },
    { label: "1차 총 결제금액", value: "20547311" },
    { label: "결제상태", value: "결제완료(2023.05.16)" },
]

export type TablelistType = {
    label: string;
    value: string | Array<{imgName: string; url: string}>;
}