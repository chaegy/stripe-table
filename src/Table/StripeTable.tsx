/** @jsxImportSource @emotion/react */
import React from "react";
import { Interpolation, Theme } from "@emotion/react";
import { TablelistType } from "../data";

type CSSTtype = {
  css?: Interpolation<Theme>
}

interface TableProps {
  tableList: Array<TablelistType>;
  tableRow: CSSTtype["css"];
  imgCell?: CSSTtype["css"];
  cell: CSSTtype["css"];
}

export default function StripeTable({tableList, tableRow, imgCell, cell}: TableProps){
  return (
    <table>
      {tableList.map(({label, value})=>{
        return (
        <tr css={tableRow} key={label}>
            <th css={headerCell}>{label}</th>
            {Array.isArray(value)
              ? <td css={cell}>
                  {value.map(({imgName, url})=>{
                    return <a href={url} download={imgName} key={imgName} css={imgCell}>{imgName}</a>
                  })}
                </td>
              : <td css={cell}>{value}</td>
            }
          </tr>
          )
        })
      } 
    </table>
  );
}

const headerCell = {
  flex: 1,
  fontWeight: "600",
  marginBottom: "auto",
  textAlign: "left" as const
}