/** @jsxImportSource @emotion/react */
import React from "react";
import { ESTIMATEDATA } from "../data";
import StripeTable from "./StripeTable";

export default function EstimateTable(){
  return (
    <section css={layout}>
      <p css={titleStyle}>견적 요청 정보</p>
      <StripeTable
        tableList={ESTIMATEDATA}
        tableRow={tableRow}
        imgCell={imgCell}
        cell={cell}
      />
    </section>
  )
}

const layout = {
  width: "auto",
  height: "100%",
  padding: "100px 0",
}
  
const titleStyle = {
  width: "600px",
  margin: "auto",
  padding: "15px 0",
  fontSize: "19px",
  fontWeight: "700",
}

const tableRow = {
  display: "flex",
  width: "600px",
  margin: "auto",
  padding: "15px 0",
  fontSize: "14px",
  borderTop: "1px solid #ddd",
  "&:first-of-type": {
    borderTop: "1px solid #000",
  },
  "&:last-child": {
    borderBottom: "1px solid #ddd"
  },
}

const cell = {
  flex: 2,
  display: "flex",
  gap: "10px",
  fontWeight: "300",
  flexDirection: "column" as const,
}

const imgCell = {
  "&:hover": {
    cursor: "pointer",
    textDecoration: "underline",
  },
}