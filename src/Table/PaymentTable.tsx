/** @jsxImportSource @emotion/react */
import React from "react";
import { PAYMENTDATA } from "../data";
import StripeTable from "./StripeTable";

export default function PaymentTable(){
  return (
    <section css={layout}>
      <p css={titleStyle}>1차 결제 정보</p>
      <StripeTable
        tableList={PAYMENTDATA}
        tableRow={tableRow} 
        cell={cell}
      />
    </section>
  )
}

const layout = {
  width: "auto",
  height: "100%",
  paddingBottom: "100px",
}
  
const titleStyle = {
  width: "600px",
  margin: "auto",
  padding: "15px 0",
  fontSize: "19px",
  fontWeight: "700",
}

const tableRow = {
  display: "flex",
  width: "600px",
  margin: "auto",
  padding: "14px 0",
  fontSize: "14px",
  "&:first-of-type": {
    borderTop: "1px solid #000",
  },
  "&:last-child": {
    borderBottom: "1px solid #ddd"
  },
}

const cell = {
  fontWeight: "300",
  textAlign: "right" as const,
}
